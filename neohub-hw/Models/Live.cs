﻿using System;
using System.Collections.Generic;
using System.Text;

namespace neohub_hw.Models
{

    public class Live
    {
        public int CLOSE_DELAY { get; set; }
        public bool COOL_INPUT { get; set; }
        public int HOLIDAY_END { get; set; }
        public bool HUB_AWAY { get; set; }
        public bool HUB_HOLIDAY { get; set; }
        public int HUB_TIME { get; set; }
        public int OPEN_DELAY { get; set; }
        public int TIMESTAMP_DEVICE_LISTS { get; set; }
        public int TIMESTAMP_ENGINEERS { get; set; }
        public int TIMESTAMP_PROFILE_0 { get; set; }
        public int TIMESTAMP_PROFILE_COMFORT_LEVELS { get; set; }
        public int TIMESTAMP_PROFILE_TIMERS { get; set; }
        public int TIMESTAMP_PROFILE_TIMERS_0 { get; set; }
        public int TIMESTAMP_RECIPES { get; set; }
        public int TIMESTAMP_SYSTEM { get; set; }
        public DeviceLive[] devices { get; set; }
    }

    public class DeviceLive
    {
        public int ACTIVE_LEVEL { get; set; }
        public int ACTIVE_PROFILE { get; set; }
        public string ACTUAL_TEMP { get; set; }
        public string[] AVAILABLE_MODES { get; set; }
        public bool AWAY { get; set; }
        public bool COOL_MODE { get; set; }
        public bool COOL_ON { get; set; }
        public int COOL_TEMP { get; set; }
        public float CURRENT_FLOOR_TEMPERATURE { get; set; }
        public string DATE { get; set; }
        public int DEVICE_ID { get; set; }
        public string FAN_CONTROL { get; set; }
        public string FAN_SPEED { get; set; }
        public bool FLOOR_LIMIT { get; set; }
        public string HC_MODE { get; set; }
        public bool HEAT_MODE { get; set; }
        public bool HEAT_ON { get; set; }
        public int HOLD_COOL { get; set; }
        public bool HOLD_OFF { get; set; }
        public bool HOLD_ON { get; set; }
        public int HOLD_TEMP { get; set; }
        public string HOLD_TIME { get; set; }
        public bool HOLIDAY { get; set; }
        public bool LOCK { get; set; }
        public bool LOW_BATTERY { get; set; }
        public bool MANUAL_OFF { get; set; }
        public bool MODELOCK { get; set; }
        public int MODULATION_LEVEL { get; set; }
        public bool OFFLINE { get; set; }
        public string PIN_NUMBER { get; set; }
        public bool PREHEAT_ACTIVE { get; set; }
        public int PRG_TEMP { get; set; }
        public bool PRG_TIMER { get; set; }
        public string[] RECENT_TEMPS { get; set; }
        public string SET_TEMP { get; set; }
        public bool STANDBY { get; set; }
        public string SWITCH_DELAY_LEFT { get; set; }
        public bool TEMPORARY_SET_FLAG { get; set; }
        public bool THERMOSTAT { get; set; }
        public string TIME { get; set; }
        public bool TIMER_ON { get; set; }
        public bool WINDOW_OPEN { get; set; }
        public int WRITE_COUNT { get; set; }
        public string ZONE_NAME { get; set; }
    }

}

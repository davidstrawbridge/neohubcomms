﻿using ConsoleTables;
using neohub_hw.Models;
using Newtonsoft.Json;
using System;
using System.Net.Sockets;
using System.Text;
using System.IO;

namespace neohub_hw
{
    class Program
    {
        // Sorry Chris - I wanted to try Dragonfruit!
        // (see https://www.hanselman.com/blog/dragonfruit-and-systemcommandline-is-a-new-way-to-think-about-net-console-apps)
        static void Main(string ipAddress = "192.168.1.149")
        {
            using (var client = new NeohubClient())
            {
                client.Open(ipAddress, 4242);

                while (true)
                {
                    Console.Clear();

                    //GetLiveData(socket);
                    GetInfoData(client);
                    //GetStatisticsData(socket);

                    for (int a = 30; a >= 0; a--)
                    {
                        Console.Write("\r Refreshing data in {0:00}", a);
                        System.Threading.Thread.Sleep(1000);
                    }
                }
            }
        }

        private static void GetLiveData(NeohubClient client)
        {
            var response = client.GetData("{\"GET_LIVE_DATA\":0}");
            var live = JsonConvert.DeserializeObject<Live>(response);

            var table = new ConsoleTable("ZONE_NAME", "SET_TEMP", "ACTUAL_TEMP", "HEAT_ON");
            foreach (var device in live.devices)
            {
                table.AddRow(device.ZONE_NAME,
                    device.SET_TEMP,
                    device.ACTUAL_TEMP,
                    device.HEAT_ON);
            }

            table.Write();
            Console.WriteLine();
        }

        private static void GetInfoData(NeohubClient client)
        {
            var response = client.GetData("{\"INFO\":0}");
            var info = JsonConvert.DeserializeObject<Info>(response);

            var table = new ConsoleTable("device", "CURRENT_SET_TEMPERATURE", "CURRENT_TEMPERATURE", "HEATING");
            foreach (var device in info.devices)
            {
                table.AddRow(device.device,
                    device.CURRENT_SET_TEMPERATURE,
                    device.CURRENT_TEMPERATURE,
                    device.HEATING);
            }

            Console.WriteLine($"Data updated at {DateTime.Now}");
            Console.WriteLine();
            table.Write();
        }

        private static void GetStatisticsData(NeohubClient client)
        {
            var response = client.GetData("{\"STATISTICS\":0}");
            var stats = JsonConvert.DeserializeObject<Statistics>(response);

            var table = new ConsoleTable("Place", "Week Low", "Week High", "Month Low", "Month High", "Year Low", "Year High");
            AddRecordRow(table, "Whole House", stats.houserecords);
            AddRecordRow(table, "Hallway", stats.roomrecords.Hallway);

            Console.WriteLine($"Data updated at {DateTime.Now}");
            Console.WriteLine();
            table.Write();

            table = new ConsoleTable("weekrun", "monthrun", "yearrun");
            table.AddRow(stats.runrecords.weekrun,
                stats.runrecords.monthrun,
                stats.runrecords.yearrun);
            Console.WriteLine();
            table.Write();
        }

        private static void AddRecordRow(ConsoleTable table, string location, Records houserecords)
        {
            table.AddRow(location,
                   houserecords.lowestweektemp,
                   houserecords.highestweektemp,
                   houserecords.lowestmonthtemp,
                   houserecords.highestmonthtemp,
                   houserecords.lowestyeartemp,
                   houserecords.highestyeartemp);
        }

        class NeohubClient : IDisposable
        {
            public void Open(string hostname, int port)
            {
                tcpClient = new TcpClient(hostname, port);

                var stream = tcpClient.GetStream();

                reader = new StreamReader(stream, Encoding.Latin1);
                writer = new StreamWriter(stream, Encoding.Latin1);
                writer.AutoFlush = true;
            }

            private TcpClient tcpClient;
            private StreamReader reader;
            private StreamWriter writer;

            public string GetData(string payload)
            {
                string jsonData = payload + "\0\n"; // \n seems more consistent than \r given the response

                writer.Write(jsonData);

                // sadly can't use this because it can't cope with the terminator
                //var info = System.Text.Json.JsonSerializer.DeserializeAsync<Info>(stream).Result;
                var response = new StringBuilder();
                var c = reader.Read();
                while (c != 0) // response terminated with "\n\0"
                {
                    response.Append((char)c);
                    c = reader.Read();
                }

                Console.WriteLine(response);

                return response.ToString();
            }

            public void Dispose()
            {
                tcpClient.Dispose();
            }
        }
    }
}
